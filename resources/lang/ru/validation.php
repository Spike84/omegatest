<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Параметр :attribute должен быть принят.',
    'active_url' => 'Параметр :attribute не является URL.',
    'after' => 'Параметр :attribute должен быть a date after :date.',
    'after_or_equal' => 'Параметр :attribute должен быть a date after or equal to :date.',
    'alpha' => 'В параметре :attribute должны содержаться только буквы.',
    'alpha_dash' => 'Параметр :attribute must only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'Параметр :attribute must only contain letters and numbers.',
    'array' => 'Параметр :attribute должен быть массивом.',
    'before' => 'The :attribute должен быть датой ранее :date.',
    'before_or_equal' => 'The :attribute должен быть датой ранее или равной :date.',
    'между' => [
        'numeric' => ':attribute должен быть между :min and :max.',
        'file' => ':attribute должен быть между :min and :max kilobytes.',
        'string' => 'T:attribute должен быть между :min and :max символов.',
        'array' => ':attribute должно быть между :min and :max items.',
    ],
    'boolean' => ':attribute field должен быть true or false.',
    'confirmed' => ':attribute не совпадает.',
    'date' => ':attribute не верная дата.',
    'date_equals' => ':attribute должен быть датой равной :date.',
    'date_format' => ':attribute не соответствует формату :format.',
    'different' => ':attribute and :other должны отличаться.',
    'digits' => ':attribute должен быть :digits digits.',
    'digits_между' => ':attribute должен быть между :min и :max цифрами.',
    'dimensions' => ':attribute не верное расширение изображения.',
    'distinct' => ':attribute field has a duplicate value.',
    'email' => ':attribute должен быть a действующий email адрес.',
    'ends_with' => ':attribute должно заканчиваться на: :values.',
    'exists' => 'Выбранное :attribute is invalid.',
    'file' => ':attribute должен быть файлом.',
    'filled' => ':attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute должен быть больше :value.',
        'file' => 'The :attribute должен быть больше :value kilobytes.',
        'string' => 'The :attribute должен быть больше :value символов.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'Параметр :attribute должен быть больше или равен :value.',
        'file' => 'Параметр :attribute должен быть больше или равен :value kilobytes.',
        'string' => 'Параметр :attribute должен быть больше или равен :value символов.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute должен быть an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute должен быть цифрой.',
    'ip' => 'The :attribute должен быть a valid IP address.',
    'ipv4' => 'The :attribute должен быть a valid IPv4 address.',
    'ipv6' => 'The :attribute должен быть a valid IPv6 address.',
    'json' => 'The :attribute должен быть a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute должен быть меньше чем :value.',
        'file' => 'The :attribute должен быть меньше чем :value kilobytes.',
        'string' => 'The :attribute должен быть меньше чем :value символов.',
        'array' => 'The :attribute must have меньше чем :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute должен быть меньше или равен :value.',
        'file' => 'The :attribute должен быть меньше или равен :value kilobytes.',
        'string' => 'The :attribute должен быть меньше или равен :value символов.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute must not be больше :max.',
        'file' => 'The :attribute must not be больше :max kilobytes.',
        'string' => 'The :attribute must not be больше :max символов.',
        'array' => 'The :attribute must not have more than :max items.',
    ],
    'mimes' => 'The :attribute должен быть a file of type: :values.',
    'mimetypes' => 'The :attribute должен быть a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute должен быть at least :min.',
        'file' => 'The :attribute должен быть at least :min kilobytes.',
        'string' => 'Параметр :attribute должен быть не менее :min символов.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'multiple_of' => 'The :attribute должен быть a multiple of :value.',
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute должен быть a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field должен быть present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'prohibited_if' => 'The :attribute field is prohibited when :other is :value.',
    'prohibited_unless' => 'The :attribute field is prohibited unless :other is in :values.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute должен быть :size.',
        'file' => 'The :attribute должен быть :size kilobytes.',
        'string' => 'The :attribute должен быть :size символов.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute должен быть a string.',
    'timezone' => 'The :attribute должен быть a valid zone.',
    'unique' => 'Такой :attribute уже есть. Выберите пожалуйста другой',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute должен быть a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
