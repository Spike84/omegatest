<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success_register' => 'Вы успешно зарегистрировались на сайте. Менеджер нашего магазина должен активировать вашу учетную запись чтобы вы смогли осуществить заказ.',
	'failed' => 'Введены не верные данные или пользователь еще не активирован администратором',
    'password' => 'НВведены не верные данные или пользователь еще не активирован администратором',
    'throttle' => 'Вами было введены не верные данные для входа слишком часто. Пожалуйста повторите попытку через :seconds секунд.',
    'login_field' => 'Логин',
    'email_field' => 'E-mail',
    'pass_field' => 'Пароль',
    'pass_repeat_field' => 'Пароль (повторно)',
    'fio_field' => 'ФИО',
    'company_field' => 'Организация',
    'tel_field' => 'Телефон',
    'city_field' => 'Город',
    'po_field' => 'Индекс',
    'adress_field' => 'Адрес',
    'region_field' => 'Регион',
    
    'login_placehold' => 'test777',
	'email_placehold' => 'info@domain.ru',
	'fio_placehold' => 'Иванов Иван Иванович',
    'company_placehold' => 'НКО Православный Храм',
    'tel_placehold' => '+74950000000',
    'city_placehold' => 'Москва',
    'po_placehold' => '100200',
    'adress_placehold' => 'Центральная ул., д. 1, пом. 22',
    
    'login_button' => 'Авторизация',
    'remember_me' => 'Запомнить меня',
    'forgot_pass' => 'Не помните пароль?',
    'register' => 'Регистрация',
    'allready_registered' => 'Авторизация',
    'reset_pass' => 'Сброс пароля',
    'required' => 'Данные поля обязательны для заполнения',

];
