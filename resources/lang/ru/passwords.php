<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль был обновлен!',
    'sent' => 'На ваш почтовый ящик отправлена ссылка для сброса пароля!',
    'throttled' => 'Необходимо подождать некоторое время перед повторной попыткой.',
    'token' => 'Не верная ссылка для смены пароля.',
    'user' => "Такого пользователя не зарегистрировано в системе",

];
