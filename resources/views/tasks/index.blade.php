@extends('layouts.app')

@section('title', 'ToDo List')
@section('description', 'ToDo List')

@section('content')

<div class="container">

	<h2>ToDo List</h2>

	<p>&nbsp;</p>

	<div class="row">
		<div class="col text-center">
			<a class="act_butt" href="{{ route('tasks.create') }}">новая задача</a>
		</div>
	</div>

	<p>&nbsp;</p>

	@if(count($tasks) > 0)
		<div class="row">
			<div class="col">
				{!! $tasks->appends(Request::except('page'))->render() !!}
			</div>
			<div class="col text-end">
				Всего задач: {{ $tasks->total() }}
			</div>
		</div>

		<hr />

		@foreach ($tasks as $task)
			<div class="row">
				<div class="col">
					<div>
						<strong>{{ $task->name }} (#{{ $task->id }})</strong><br />
						{{ $task->conts }}<br />
					</div>
					<div class="text-muted text-end">
						Создано: {{ $task->created_at }}<br />
						Обновлено: {{ $task->updated_at }}
					</div>
					<div class="actions">
						<a class="act_butt" title="Редактировать" href="{{ route('tasks.edit', $task->id) }}">редактировать</a>

						<form action="{{ route('tasks.destroy', ['task' => $task->id]) }}"
							method="post" onsubmit="return confirm('Вы действительно хотите удалить задачу {{ $task->id }} ?')">
							@csrf
							@method('DELETE')
								<button type="submit" title="Удалить" class="act_butt m-0 border-0 bg-transparent text-primary"><u>удалить</u></button>
						</form>
					</div>
				</div>
			</div>
			<hr />
		@endforeach

		<div class="row">
			<div class="col">
				{!! $tasks->appends(Request::except('page'))->render() !!}
			</div>
			<div class="col text-end">
				Всего задач: {{ $tasks->total() }}
			</div>
		</div>
	@else
		<div class="row">
			<div class="col">задачи еще не созданы</div>
		</div>
	@endif

	<p>&nbsp;</p>

	<div class="row">
		<div class="col text-center">
			<a class="act_butt" href="{{ route('tasks.create') }}">новая задача</a>
		</div>
	</div>

	<p>&nbsp;</p>

</div>
@endsection
