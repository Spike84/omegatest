@extends('layouts.app')

@section('title', 'Новая Задача')
@section('description', 'Новая Задача')

@section('content')
	<div>
		<h2>Новая Задача</h2>

		<form method="post" action="{{ route('tasks.store') }}">
	        @csrf
		    
		    <div class="container">
		    	<div class="row">
		    		<div class="col mt-2">
		    			Название
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col">
		    			<input type="text" name="name" class="form-group w-100" value="{{ old('name') }}" />
		    		</div>
		    	</div>
		    	<div class="row mt-2">
		    		<div class="col">
		    			Описание
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col">
		    			<textarea name="conts" class="form-control" rows="12">{{ old('conts') }}</textarea>
		    		</div>
		    	</div>
		    </div>

			<p>&nbsp;</p>

			<div class="container buttons_block">
	        	<div class="row text-center justify-content-center">
	        		<div class="buttons_block_sub" style="width: 400px;">

	        			<div class="container">
	        				<div class="row">
								<div class="col">
						            <button class="" type="submit">Сохранить</button>
						        </div>
						        <div class="col"><a href="{{ route('tasks.index') }}">Отмена</a></div>
						    </div>
						</div>

			    	</div>
				</div>
	        </div>
	        
	    </form>
    </div>
@endsection
