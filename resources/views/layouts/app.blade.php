<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="@yield('description')">

        <script>document.getElementsByTagName("html")[0].className += " js";</script>

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/fancybox.css') }}">
        <link rel="stylesheet" href="{{ asset('/js/timeline/css/style.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('/js/app.js') }}" defer></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="{{ asset('/js/fancybox-1.2.6.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?skin=default"></script>
        <script src="{{ asset('/js/site.js') }}"></script>
    </head>
    <body>
        <div class="wrapper">
            <header>
                @include('layouts.elements.menu')
            </header>
            
            <!-- Page Content -->
             <div class="container">
				<div class="row">
					<div class="col">
						@include('layouts.elements.mess')
                        @yield('content')
                    </div>
                 </div>
             </div>
             
            <div class="empty">&nbsp;</div>
        </div>
        <footer id="footer" class="footer navbar-fixed-bottom">
            <div class="container">
                <div class="text-center">Property of ProgPHP group. All rights reserved &copy; @php echo date('Y'); @endphp</div>
            </div>
        </footer>
        <!--script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous" async></script-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
        <script src="{{ asset('/js/timeline/js/util.js') }}"></script>
		<script src="{{ asset('/js/timeline/js/swipe-content.js') }}"></script>
		<script src="{{ asset('/js/timeline/js/main.js') }}"></script>
    </body>
</html>
