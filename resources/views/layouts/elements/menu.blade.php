<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
    	<a class="nav-link text-center @if(isset($here) && $here=='tasks') active @endif" href="/">Главная</a>
      </div>
    </div>
    <div class="container text-center" style="max-width: 200px;">
		<a class="navbar-brand" href="/">
			<img src="/img/logo_w.png" width="120" height="40" class="d-inline-block align-top" alt="" loading="lazy">
		</a>
    </div>
  </div>
</nav>