@if (session('status') || session('error') || $errors->any())
    <div class="container">
         <div class="row">
                <div class="col">
                    @if (session('status'))
                    <div class="alert bg-success text-light">
                        {{ session('status') }}
                    </div>
                    @endif
                    @if (session('error'))
                        <div class="alert bg-danger text-light">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert bg-danger text-light">{!! implode('<br />', $errors->all(':message')) !!}</div>
                    @endif
                </div>
          </div>
    </div>
@endif
