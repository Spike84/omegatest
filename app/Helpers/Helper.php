<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use App\Models\Importlog;

class Helper
{
	public static function import_file_inf(string $dir, string $file)
    {
        $obj = new Helper();
        
        $obj->type = 'файл';

		if(empty($file)) {
			$obj->type = 'директория';
		}

        if (Storage::disk('import')->exists($dir.$file)) {
        	$obj->ready = 1;

        	if($obj->type == 'файл') {
        		$obj->conts = file(Storage::disk('import')->path($dir.$file));
        		$obj->num = (count($obj->conts)-1);
        	} elseif ($obj->type == 'директория') {
        		$obj->conts = Storage::disk('import')->files($dir);
        		$obj->num = count($obj->conts);
        	}

        	if ($obj->num == 0) {
	        	$obj->ready = 0;
	        	$obj->result = "\n<br />".date('Y-m-d H:i:s'). " - ".$obj->type." по пути ".$dir.$file." не содержит элементов для выгрузки";
	        } else {
	        	$obj->result = "\n<br />".date('Y-m-d H:i:s'). " - ".$obj->type." по пути ".$dir.$file." содержит ".$obj->num." элементов для выгрузки";
	        }

        } else {
        	$obj->ready = 0;
        		$obj->result = "\n<br />".date('Y-m-d H:i:s'). " - не найдено: ".$obj->type." по пути ".$dir.$file;
        }
        
        return $obj;
    }

	public static function image_resizer(string $file, string $width)
    {
    	if (Storage::disk('public')->exists($file)) {
		    $fileinf = pathinfo($file);
		    
		    $res_file = $fileinf['dirname']."/resized/".$fileinf['filename']."/".$fileinf['filename']."x".$width.".".$fileinf['extension'];

		    if (Storage::disk('public')->exists($res_file)) {
		    	return Storage::disk('public')->url($res_file);
		    } else {
		    	Storage::disk('public')->copy($file, $res_file);
		    	
		    	$base_file = Storage::disk('public')->path($file);
		    	$new_file = Storage::disk('public')->path($res_file);
		    	
		    	$imgsze = getimagesize($base_file);

		    	if($imgsze[0] <= $width) {
		    		return Storage::disk('public')->url($res_file);
		    	} else {
			    	if ($imgsze['mime'] == "image/jpeg" || $imgsze['mime'] == "image/pjpeg") {
			    		$im = imagecreatefromjpeg($base_file);
			    		$new_height = round($width*$imgsze[1]/$imgsze[0]);
			    		$dest = imagecreatetruecolor($width,$new_height);
			    		imagecopyresampled($dest, $im, 0, 0, 0, 0, $width, $new_height, $imgsze[0], $imgsze[1]);

						$mimg = imagejpeg($dest, $new_file, '100');
						//$mimg = Storage::disk('public')->put($new_file, $dest);
						imagedestroy($dest);
						imagedestroy($im);
			    	} elseif ($imgsze['mime'] == "image/gif") {
			    		//$im = imagecreatefromgif($base_file);

			    		
			    	} elseif ($imgsze['mime'] == "image/png") {
			    		//$im = imagecreatefrompng($base_file);

			    		
			    	}
			    	
			    	return Storage::disk('public')->url($res_file);
		    	}

		    }
		    //print_r($fileinf);
		    //return $res_file;
		} else {
			return "файл не найден";
		}
    }

	public static function check_importlog_exists(string $id, array $cond, string $child)
    {
    	if (!empty($cond)) {
		    $importlog = Importlog::where([
	        	['id', '=', $id],
	        	$cond,
	        ])->first();
        } else {
        	$importlog = Importlog::find($id);
        }

		if (!$importlog) {
			echo redirect()->route('admin.importlogs.index')
				->with('error','Не найдено');
		}

		if($child && $importlog->$child->count() == 0) {
			echo redirect()->route('admin.importlogs.index')
				->with('error','Не найдено элементов в выгрузке');
		}

		return $importlog;
    }

	public static function get_blogDate(string $date)
    {
    	$months = array(1 => 'Янв' , 'Фев' , 'Мар' , 'Апр' , 'Май' , 'Июн' , 'Июл' , 'Авг' , 'Сен' , 'Окт' , 'Ноя' , 'Дек' );
    	$day =  $months[date('n', strtotime($date))]."<br /> ".date('Y', strtotime($date));

    	return $day;
    }

}