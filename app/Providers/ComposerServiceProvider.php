<?php

namespace App\Providers;

use App\Models\Postcat;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.elements.filter', function($view) {
            $view->with(['postcats' => Postcat::itemlist()]);
        });
    }
}
