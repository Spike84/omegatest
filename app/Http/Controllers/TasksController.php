<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{
	public function index()
	{
        $tasks = Task::orderBy('created_at', 'asc')
        	->paginate(5);

        $here = "tasks";

        return view('tasks.index',compact('tasks','here'));
	}

    public function create()
    {
    	$here = "tasks";

        return view('tasks.create', compact('here'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255|min:3|unique:tasks',
        ]);

        $input = $request->all();

		try {
	        $task = Task::create($input);

	        return redirect()->route('tasks.index')
	        	->with('status','Задача #'.$task->id.' успешно создана');
		} catch(\Illuminate\Database\QueryException $ex) {
			return redirect()->route('tasks.index')
				->with('error','Ошибка создания задачи #'.$task->id);
		}
    }

    public function edit($id)
    {
        $task = Task::findOrFail($id);

		$here = "tasks";

        return view('tasks.edit', compact('task','here'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255|min:3|unique:tasks,name,'.$id,
        ]);

		$task = Task::findOrFail($id);

        $input = $request->all();
		
		try {
        	$task->update($input);

        	return redirect()->route('tasks.index')
				->with('status','Задача #'.$task->id.' успешно обновлена');
        } catch(\Illuminate\Database\QueryException $ex) {
        	return redirect()->route('tasks.index')
				->with('error','Ошибка обновления задачи #'.$task->id);
        }
    }

    public function destroy($id)
    {
        $task = Task::select('id')->findOrFail($id);

		try {
	        $task->delete();

	        return redirect()->route('tasks.index')
				->with('status','Задача #'.$task->id.' успешно удалена');
		} catch(\Illuminate\Database\QueryException $ex) {
			return redirect()->route('tasks.index')
				->with('error','Ошибка удаления задачи #'.$task->id);
		}
    }
}
