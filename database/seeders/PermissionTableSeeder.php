<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'user-list',
           'user-edit',
           'user-delete',
           'post-list',
           'post-show',
           'post-create',
           'post-edit',
           'post-delete',
           'region-list',
           'region-create',
           'region-edit',
           'region-delete',
           'user-show',
           'page-list',
           'page-create',
           'page-edit',
           'page-delete',
           'postcat-list',
           'postcat-create',
           'postcat-edit',
           'postcat-delete',
           'img-list',
           'img-create',
           'img-delete',
           'project-list',
           'project-create',
           'project-show',
           'project-edit',
           'project-delete',
           'access-admin'
        ];

		$exist_perm = Permission::all('name');
		foreach($exist_perm as $item) 
		{
			$permissions['exists'][] = $item->name;
		};

        foreach ($permissions as $key=>$val)
        {
             if ($key !='exists' && !in_array($val,$permissions['exists'])) {
             	Permission::create(['name' => $val]);
             }
        };
    }
}
