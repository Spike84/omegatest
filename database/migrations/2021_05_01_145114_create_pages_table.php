<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->integer('sort')->nullable();
            $table->string('url', 255);
            $table->string('menu', 255);
            $table->string('name', 255)->nullable();
            $table->string('h1', 255)->nullable();
            $table->text('desc')->nullable();
            $table->text('conts')->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
